# Nagios Plugins

Useful Nagios plugins that I've created.

- **check_public_ip**: Perform a DNS lookup of a public host and compare it to the IP address retrieved from an EdgeRouter gateway.  Very useful for detecting when an ISP has changed your public IP address.

- **check_updateinfo**: Use yum or dnf to check for updates.

